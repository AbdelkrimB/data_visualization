@echo off
SET find_one_param = 0

if [%1].==. GOTO NoParam

for %%a in (html pdf singlehtml docx all) do (
    
    if %1==%%a (
       call _scripts/build.%1.bat
       SET find_one_param = 1
       echo %%a
    )
)

If %find_one_param%==1 GOTO End1

echo "Parameter is invalid!"
GOTO NoParam


:NoParam
    echo "usage : build.bat html | pdf | singlehtml | docx | all"
GOTO End1

:End1
