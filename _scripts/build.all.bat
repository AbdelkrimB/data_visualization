echo on
echo version 2018-12-01 
rd build /s /q
make html && make singlehtml && mkdir build\docx && cd build\singlehtml  && pandoc .\index.html -f html -t docx -o .\index.docx --verbose && move .\index.docx ..\docx\ && cd ..\.. && make latexpdf
echo 'finished!'ls -la