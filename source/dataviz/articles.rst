Articles and Blogs
==================

Articles and Blog posts describing how to increase the quality of your dashboards

Open collaboration on COVID-19 
    https://github.blog/2020-03-23-open-collaboration-on-covid-19
    By Martin Woodward 

Live tracker: How many coronavirus cases have been reported in each U.S. state?
    https://www.politico.com/interactives/2020/coronavirus-testing-by-state-chart-of-new-cases/  
    By Beatrice Jin | 03/16/2020 8:10 PM EDT | Updated 04/22/2020 7:10 PM EDT

Closing the data divide: the need for open data, Apr 21, 2020 | Jennifer Yokoyama - Chief IP Counsel
    https://blogs.microsoft.com/on-the-issues/2020/04/21/open-data-campaign-divide/ 

Bing delivers new COVID-19 experiences including partnership with GoFundMe to help affected businesses
    https://blogs.bing.com/search/2020_04/Bing-delivers-new-COVID-19-experiences-including-partnership-with-GoFundMe-to-help-affected-business 

17 (or so) responsible live visualizations about the coronavirus, for you to use
    https://blog.datawrapper.de/coronaviruscharts

Epidemic Modeling 101: Or why your CoVID19 exponential fits are wrong
    https://github.com/DataForScience/Epidemiology101 

Charting new territory; How The Economist designs charts for Instagram
    https://medium.economist.com/charting-new-territory-7f5afb293270

