Software Architecture
=====================


Gatsby is a free and open source framework based on React that helps developers build blazing fast websites and apps: 

    https://www.gatsbyjs.org/


urlwatch configuration to monitor state COVID-19 data: 

    https://github.com/COVID19Tracking/covid-tracking 

urlwatch monitors webpages for you:

    https://github.com/thp/urlwatch 

The COVID Tracking Project collects and publishes the most complete testing data available for US states and territories.

    https://github.com/COVID19Tracking/website/tree/master/build

Scan/Trim/Extra Pipeline for State Coronavirus Site 

    https://github.com/COVID19Tracking/covid-data-pipeline

The crawler and parsers are there for the 50 US states and DC. The focus is to collect offical published COVID-19 statistics.

    https://github.com/coronavirusapi/crawl-and-parse

    https://coronavirusapi.com/

Provide embed code to integrate your dashboards easily

    https://github.com/microsoft/COVID-19-Widget
