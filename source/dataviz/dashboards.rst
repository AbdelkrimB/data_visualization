Dashboards
==========

.. csv-table:: Dashboards
    :header: description, dashboard url, source code, 

    WHO Coronavirus (COVID-19), https://covid19.who.int, none
    WHO - Explore the Data, https://covid19.who.int/explorer, none
    Healthmap (made by schools and hospitals), https://healthmap.org/covid-19, none
    COVID-19 India, https://www.covid19india.org, https://github.com/covid19india/covid19india-react
    COVID-19 Scenarios, https://covid19-scenarios.org, https://github.com/neherlab/covid19_scenarios
    List of dashboards WW, https://covid19dashboards.com, https://github.com/github/covid19-dashboard
    How many days each country’s outbreak is behind or ahead of the United States, https://predictcovid.com, https://github.com/lachlanjc/covid19
    COVID-19 Italia - Monitoraggio situazione (Desktop app), https://github.com/pcm-dpc/COVID-19, http://arcg.is/C1unv
    COVID-19 Italia - Monitoraggio situazione (Mobile app), https://github.com/pcm-dpc/COVID-19, http://arcg.is/081a51
    Latest updates on COVID-19 in Tokyo, https://stopcovid19.metro.tokyo.lg.jp, https://github.com/tokyo-metropolitan-gov/covid19
    Official World Health Organization COVID-19 App, https://github.com/WorldHealthOrganization/app
    Real-time tracking of pathogen evolution, https://nextstrain.org, https://github.com/nextstrain
    16,000 viral genomic sequences of hCoV-19 shared with unprecedented speed via GISAID, https://www.gisaid.org, https://www.gisaid.org/epiflu-applications/next-hcov-19-app
    Dashboard of the COVID-19 Virus Outbreak, https://co.vid19.sg/singapore, none