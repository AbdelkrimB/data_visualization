# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.9](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.8...v0.0.9) (2020-05-07)


### Documentations

* add a data model description ([1e0969d](https://bitbucket.org/altf1be/data_visualization/commit/1e0969da2640a1a629afbbd786b9e7dac66135dd))
* add datasources from https://www.codevscovid19.org/ ([f872776](https://bitbucket.org/altf1be/data_visualization/commit/f872776615b45583344d506772d7d1342ce3017f))
* add link to the latest news on the api economy ([00ea723](https://bitbucket.org/altf1be/data_visualization/commit/00ea7235c5084ce7951696a7a10f96108e2d6735))
* describe how to build and visualize the documentation ([5bc6a29](https://bitbucket.org/altf1be/data_visualization/commit/5bc6a29b8aaa7bd9a7c7a4b2fa5ecb9c042f6cc4))
* remove all TBD and set a meaningful description ([d0e70e8](https://bitbucket.org/altf1be/data_visualization/commit/d0e70e899879ddfe0197fa4dbf6cffb835654306))


### Styles

* add CR to increase the readability ([30eacc3](https://bitbucket.org/altf1be/data_visualization/commit/30eacc3ca54d75d2e53fa1358a0c0f70f35e4b70))

### [0.0.8](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.7...v0.0.8) (2020-04-30)


### Documentations

* add citations page ([91136c8](https://bitbucket.org/altf1be/data_visualization/commit/91136c8a71d20cef557fc5005b3df40893bb813b))
* add data formats  page ([91ae2e7](https://bitbucket.org/altf1be/data_visualization/commit/91ae2e79ddbb5ba5edd6bf3a553d0903a913f38f))
* add data models page ([b400f35](https://bitbucket.org/altf1be/data_visualization/commit/b400f354110b83956c06633c4f5cdd85eca85750))
* add REST APIs page ([8f03940](https://bitbucket.org/altf1be/data_visualization/commit/8f039404f595d866f6a2a39e4e9d38aed785e857))
* **style:** revise the heading of the pages ([4d913af](https://bitbucket.org/altf1be/data_visualization/commit/4d913af206134e52d2ee3b89d0b0caa71397347f))

### [0.0.7](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.6...v0.0.7) (2020-04-30)


### Documentations

* **fix:** correct the title ([0e62cd0](https://bitbucket.org/altf1be/data_visualization/commit/0e62cd02f81dcf33c802d6931b872b8f981d3ce7))

### [0.0.6](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.5...v0.0.6) (2020-04-30)


### Documentations

* add links to new articles: epidemiology and charts on instagram ([cb250c0](https://bitbucket.org/altf1be/data_visualization/commit/cb250c06a24e671a8909916d6c0eccd8c237e427))
* add Lockdowns by country and Tests conducted by country ([254ea2d](https://bitbucket.org/altf1be/data_visualization/commit/254ea2dbffb7074ae008a0ec358257721cbb5977))

### [0.0.5](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.4...v0.0.5) (2020-04-30)


### Features

* add todos' list in a page named to_do_list, set a custom.css ([0b571ae](https://bitbucket.org/altf1be/data_visualization/commit/0b571ae7e5d65b435f49b789384b51e8cfbc6e1c))
* html content page is 1024px wide ([f89bf6f](https://bitbucket.org/altf1be/data_visualization/commit/f89bf6fe56ccf67a533d3d8029f3a189b7956dc6))
* install git-lfs if the deployment is made on readthedocs.org ([2192470](https://bitbucket.org/altf1be/data_visualization/commit/21924700523c0b48fe702b4525256e0230e790eb))


### Documentations

* **fix:** change the names of the columns ([29e2a94](https://bitbucket.org/altf1be/data_visualization/commit/29e2a94c16377df5e0dd92c51273380d78b99052))
* add 2 more pages in the table of contents ([29cb53a](https://bitbucket.org/altf1be/data_visualization/commit/29cb53a27f9d22129ebc8d0054cd2cf1c79f056d))
* add RDA COVIS-19 recommandations and guidelines ([11f726e](https://bitbucket.org/altf1be/data_visualization/commit/11f726eb461eeafe649b867591907e33ab0a2393))
* display a todo' list ([66faabe](https://bitbucket.org/altf1be/data_visualization/commit/66faabe14812e6a1f9008e3ae811a88eae7d7e7f))


### Styles

* **fix:** add spaces between lines to make them readable ([66926f6](https://bitbucket.org/altf1be/data_visualization/commit/66926f68ebe4fcfdfcd6eebd3dc7013b3919a3d4))


### Builds

* set 'pandas' environment using conda ([bc655ca](https://bitbucket.org/altf1be/data_visualization/commit/bc655caa58342e4c7974bf651f6d8d96e42bca93))

### [0.0.4](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.3...v0.0.4) (2020-04-26)


### Features

* enable git-lfs ([859107f](https://bitbucket.org/altf1be/data_visualization/commit/859107f7d04282bd92c001e82638f2d432a4760d))


### Continous improvements (CI)

* add missing libraries on readthedocs.org such as unidecode ([a56f7ae](https://bitbucket.org/altf1be/data_visualization/commit/a56f7aecb9a93b67a04946102cf3b171674502a7))

### [0.0.3](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.1...v0.0.3) (2020-04-26)


### Documentations

* add data sources from the RDA COVID-19 Working Group, Recommendations and Guidelines, 1st release, 24 April 2020 ([65c6c2e](https://bitbucket.org/altf1be/data_visualization/commit/65c6c2e233a05f46757b462fd6a0474732428f84))


### Chores

* first commit between github and bitbucket ([fbb6fa0](https://bitbucket.org/altf1be/data_visualization/commit/fbb6fa0fd485720b35c73fe948122cbbf8085109))
* **release:** 0.0.2 ([31bb63c](https://bitbucket.org/altf1be/data_visualization/commit/31bb63c0d9386372fca8c6d1f40b1f98498a3651))

### [0.0.2](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.1...v0.0.2) (2020-04-25)


### Chores

* first commit between github and bitbucket ([fbb6fa0](https://bitbucket.org/altf1be/data_visualization/commit/fbb6fa0fd485720b35c73fe948122cbbf8085109))

### [0.0.1](https://bitbucket.org/altf1be/data_visualization/compare/v0.0.2...v0.0.1) (2020-04-25)


### Documentations

* first commit of the book : Data Visualization during outbreaks like COVID-19 ([60aaef5](https://bitbucket.org/altf1be/data_visualization/commit/60aaef5c895612a9a272885113c7bfc7eb080f7a))

### 0.0.2 (2020-04-25)

### 0.0.1 (2020-04-25)
